package com.example.rootsfood;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RootsFoodApplication {

    public static void main(String[] args) {
        SpringApplication.run(RootsFoodApplication.class, args);
    }

}
